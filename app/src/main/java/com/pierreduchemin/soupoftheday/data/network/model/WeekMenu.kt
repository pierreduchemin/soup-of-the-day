package com.pierreduchemin.soupoftheday.data.network.model

import pl.droidsonroids.jspoon.annotation.Selector
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class WeekMenu {

    @Selector(value = ".entry-title", regex = "Menu de la semaine du (\\d+\\s\\w+) au \\d+\\s\\w+")
    var startDateStr: String = ""
    @Selector(".entry-title")
    var title = ""
    @Selector(value = ".entry-content p", regex = "(\\w+:.*)")
    var content = ArrayList<String>()

    fun getWeekManuDate(): Date? {
        if (!startDateStr.isEmpty()) {
            val format = SimpleDateFormat("d MMMM", Locale.FRENCH)
            return format.parse(startDateStr)
        }
        return null
    }
}

