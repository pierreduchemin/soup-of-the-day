package com.pierreduchemin.soupoftheday.data

import com.pierreduchemin.soupoftheday.data.network.model.WeekMenu

interface MenuDataSource {

    fun getAllMenus(callback: GetAllMenuCallback)

    fun getLastMenu(callback: GetAllMenuCallback)

    interface GetLastMenuCallback {

        fun onMenuLoaded(weekMenu: WeekMenu)

        fun onDataNotAvailable()
    }

    interface GetAllMenuCallback {

        fun onMenusLoaded(weekMenus: List<WeekMenu>)

        fun onDataNotAvailable()
    }
}