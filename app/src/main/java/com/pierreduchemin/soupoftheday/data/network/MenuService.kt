package com.pierreduchemin.soupoftheday.data.network

import com.pierreduchemin.soupoftheday.data.network.model.Lang
import com.pierreduchemin.soupoftheday.data.network.model.MenuPage

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface MenuService {
    @GET("/")
    fun getMenuPage(@Query("lang") lang: Lang = Lang.EN): Call<MenuPage>
}